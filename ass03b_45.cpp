#include "bits/stdc++.h"
#include "sys/shm.h"
#include "unistd.h"
#define QUANTUM 1
#define VEC_SIZE 1000
using namespace std;
int isDead = 0;
vector <int> status;
vector <pthread_t> threads;
void handler (int sign) {
    if (sign == SIGUSR1)
        pause();
    signal(SIGUSR1, handler);
    signal(SIGUSR2, handler);
}

void *worker(void *ptr) {
    int *no = (int*)ptr;
    vector <int> vec;
    int iter;
    struct timespec tim, tim2;
    tim.tv_sec = rand() % 10;
    tim.tv_nsec = 0;
    signal(SIGUSR1, handler);
    signal(SIGUSR2, handler);
    pause();
    srand ((unsigned int)time(NULL));
    for (iter = 0; iter < VEC_SIZE; iter ++){
        vec.push_back(rand());
    }
    sort(vec.begin(), vec.end());
    while (tim.tv_nsec > 0 || tim.tv_sec > 0) {
        if(nanosleep(&tim , &tim) < 0 ) {
            printf("    +++ Remaining time for Thread %d is %d seconds and %lld nanoseconds\n", *no + 1, (int)tim.tv_sec, (long long int)tim.tv_nsec);
        } else {
            isDead = 1;
            printf("    !!! Thread %d finished its job.\n", *no + 1);
            pthread_exit(NULL);
        }
    }
}
void printArr (vector<int> arr) {
    int ind, size = (int) arr.size();
    for (ind = 0; ind < size; ind ++)
        cout << arr[ind] << " ";
    cout << "\n";
}

void *scheduler (void *ptr) {
    int jobIter, n = (int) status.size();
    int tempId;
    vector <int> workerId(n);
    for (jobIter = 0; jobIter < n; jobIter ++)
        workerId [jobIter] = jobIter;
    status[0] = 1;
    isDead = 0;
    pthread_kill(threads[0], SIGUSR2);
    pthread_kill(threads[n + 1], SIGUSR2);
    while (true) {
        isDead = 0;
        sleep(QUANTUM);
        if (isDead == 1) {
            status[workerId[0]] = 2;
            workerId.erase(workerId.begin());
            if (workerId.empty()) {
                pthread_kill(threads[n + 1], SIGUSR2);
                break;
            }
            status[workerId[0]] = 1;
            pthread_kill(threads[workerId[0]], SIGUSR2);
            pthread_kill(threads[n + 1], SIGUSR2);
        } else if (workerId.size() > 1) {
            tempId = workerId[0];
            pthread_kill(threads[tempId], SIGUSR1);
            status[tempId] = 0;
            workerId.erase(workerId.begin());
            workerId.push_back(tempId);
            pthread_kill(threads[workerId[0]], SIGUSR2);
            status[workerId[0]] = 1;
            pthread_kill(threads[n + 1], SIGUSR2);
        }
    }
    printf("Scheduler terminated\n");
    pthread_exit(NULL);
}

void *reporter (void *ptr) {
    signal(SIGUSR2, handler);
    int n = (int) status.size(), iter, fCount = 0;
    vector <int> fin(n, 0);
    pause();
    while (fCount < n) {
        //printArr(status);
        for (iter = 0; iter < n; iter ++) {
            if (status[iter] == 1) {
                printf(" +++ Switched to Thread %d\n", (iter + 1));
            }
            if (status[iter] == 2 && fin[iter] == 0) {
                printf(" !!! Thread %d  terminated\n", (iter + 1));
                fin[iter] = 1;
                fCount += 1;
            }
        }
        if (fCount < n) {
            pause();
        }
    }
    printf("Reporter terminated\n");
    pthread_exit(NULL);
}

int main() {
    int iter, n;
    cout << "Enter the number of concurrent threads : ";
    cin >> n;
    threads.resize((unsigned long) n + 2);
    status.resize((unsigned long) n);
    fill(status.begin(), status.end(), 0); // 0 denotes not yet finished
    int *posArr = (int*) malloc(n * sizeof(int));
    for (iter = 0; iter < n; iter ++) {
        posArr [iter] = iter;
        pthread_create(&(threads[iter]), NULL, worker, (void *)&(posArr[iter]));
    }
    pthread_create(&(threads[n]), NULL, scheduler, NULL);
    pthread_create(&(threads[n + 1]), NULL, reporter, NULL);
    for (iter = 0; iter < n + 2; iter ++) {
        pthread_join(threads[iter], NULL);
    }
}
